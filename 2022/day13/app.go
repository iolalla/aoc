package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"
	"strings"
)

func main() {
	//input, _ := os.ReadFile("test.txt")
	input, _ := os.ReadFile("input.txt")
	//Part1
	pkts, part1 := []any{}, 0
	for i, s := range strings.Split(strings.TrimSpace(string(input)), "\n\n") {
		s := strings.Split(s, "\n")
		var a, b any
		json.Unmarshal([]byte(s[0]), &a)
		json.Unmarshal([]byte(s[1]), &b)
		pkts = append(pkts, a, b)

		if cmp(a, b) <= 0 {
			part1 += i + 1
		}
	}
	fmt.Println(part1)
	//Part2
	pkts = append(pkts, []any{[]any{2.0}}, []any{[]any{6.0}})
	sort.Slice(pkts, func(i, j int) bool { return cmp(pkts[i], pkts[j]) < 0 })

	part2 := 1
	for i, p := range pkts {
		if fmt.Sprint(p) == "[[2]]" || fmt.Sprint(p) == "[[6]]" {
			part2 *= i + 1
		}
	}
	fmt.Println(part2)
}

func cmp(a, b any) int {
	var al, bl []any
	aok, bok := false, false

	switch a.(type) {
	case float64:
		al, aok = []any{a}, true
	case []any, []float64:
		al = a.([]any)
	}

	switch b.(type) {
	case float64:
		bl, bok = []any{b}, true
	case []any, []float64:
		bl = b.([]any)
	}

	if aok && bok {
		return int(al[0].(float64) - bl[0].(float64))
	}
	for i := range al {
		if i >= len(bl) {
			break
		} else if c := cmp(al[i], bl[i]); c != 0 {
			return c
		}
	}
	return len(al) - len(bl)
}
