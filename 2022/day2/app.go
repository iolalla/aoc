package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)
	// A for Rock, B for Paper, and C for Scissors.
	// X for Rock, Y for Paper, and Z for Scissors.
	/**
	In the first round, your opponent will choose Rock (A), and you should choose Paper (Y). This ends in a win for you with a score of 8 (2 because you chose Paper + 6 because you won).
	In the second round, your opponent will choose Paper (B), and you should choose Rock (X). This ends in a loss for you with a score of 1 (1 + 0).
	The third round is a draw with both players choosing Scissors, giving you a score of 3 + 3 = 6.
	In this example, if you were to follow the strategy guide, you would get a total score of 15 (8 + 1 + 6).

	Rock 1
	Paper 2
	Scissors 3

	6 win + token
	3 draw + token
	0 Loss + token
	*/
	fileScanner.Split(bufio.ScanLines)
	var points int
	for fileScanner.Scan() {
		values := strings.Split(fileScanner.Text(), " ")
		if values[0] == "A" {
			// A for Rock
			// Rock == 1
			if values[1] == "Y" { //Paper
				points = points + 2 + 6
			} else if values[1] == "Z" { //Scissors
				points = points + 3
			} else if values[1] == "X" { //Rock
				points = points + 1 + 3
			} else {
				fmt.Printf("This is impossible: %s should be X, Y or Z", values[1])
			}
		} else if values[0] == "B" {
			//B for Paper
			if values[1] == "Y" { //Paper
				points = points + 2 + 3
			} else if values[1] == "Z" { //Scissors
				points = points + 3 + 6
			} else if values[1] == "X" { //Rock
				points = points + 1
			} else {
				fmt.Printf("This is impossible: %s should be X, Y or Z", values[1])
			}
		} else if values[0] == "C" {
			//C for Scissors
			if values[1] == "Y" { //Paper
				points = points + 2
			} else if values[1] == "Z" { //Scissors
				points = points + 3 + 3
			} else if values[1] == "X" { //Rock
				points = points + 1 + 6
			} else {
				fmt.Printf("This is impossible: %s should be X, Y or Z", values[1])
			}
		} else {
			fmt.Printf("This is impossible: %s should be A, B or C", values[0])
		}
	}
	readFile.Close()
	fmt.Printf("Final Result %d", points)
}
