package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/**
North = "^"
East  = ">"
South = "v"
West  = "<"
*/

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var input []string
	for fileScanner.Scan() {
		input = append(input, fileScanner.Text())
	}
	readFile.Close()

	commands := strings.Split(input[0], "")
	houses := map[string]bool{"0-0": true}
	coords := []int{0, 0}

	for _, c := range commands {
		y, x := coords[0], coords[1]

		switch c {
		case "^":
			y--
		case ">":
			x++
		case "v":
			y++
		case "<":
			x--
		}

		coords = []int{y, x}
		houses[fmt.Sprintf("%d-%d", y, x)] = true
	}

	fmt.Printf("Solution: %v\n", len(houses))

}
