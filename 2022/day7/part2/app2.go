package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
	"unicode"
)

type file struct {
	name   string
	size   int
	isDir  bool
	parent *file
	files  []*file
}

func (f *file) addFile(name string, size int) {
	f.files = append(f.files, &file{name: name, parent: f, size: size})
	f.increaseSize(size)
}

func (f *file) addDir(name string) {
	f.files = append(f.files, &file{name: name, parent: f, files: []*file{}, isDir: true})
}

func (f *file) increaseSize(size int) {
	f.size += size
	if f.parent != nil {
		f.parent.increaseSize(size)
	}
}

func (f *file) findDir(name string) *file {
	for _, d := range f.files {
		if d.name == name && d.isDir {
			return d
		}
	}
	return nil
}

func main() {
	readFile, err := os.Open("../input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 *
	 */
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var lastPath string
	var lastDir string
	root := &file{name: "/", files: []*file{}}
	current := root
	for fileScanner.Scan() {
		//
		var size int
		var name string
		var dir string
		value := fileScanner.Text()
		if strings.HasPrefix(value, "$ cd") {
			fmt.Sscanf(value, "$ cd  %s", &dir)
			if dir == "/" {
				current = root
				// continue
			} else if dir == ".." {
				lastPath = lastPath + "/" + dir
				lastDir = dir
				current = current.parent
			} else {
				current = current.findDir(dir)
				lastPath = strings.Replace(lastPath, "/"+lastDir, "", -1)
			}
		}
		if strings.HasPrefix(value, "dir ") {
			fmt.Sscanf(value, "dir %s", &dir)
			current.addDir(dir)
		}
		if unicode.IsDigit(rune(value[0])) {
			fmt.Sscanf(value, "%d %s", &size, &name)
			//fmt.Printf("%s , %d \n", lastPath, size)
			//add(lastPath, size)
			current.addFile(name, size)
		}
	}
	readFile.Close()
	unusedSpace := 70000000 - root.size
	minDirSpace := 30000000 - unusedSpace
	candidates := findCandidates(root, minDirSpace)

	// Sort from min size to max size
	sort.Slice(candidates, func(i, j int) bool {
		return candidates[i].size < candidates[j].size
	})

	fmt.Printf("Answer: %d", candidates[0].size)
}

func findCandidates(directory *file, minSize int) []*file {
	dirs := make([]*file, 0)

	if directory.size >= minSize { // this is the size we need to free up
		dirs = append(dirs, directory)
	}

	for _, subdir := range directory.files {
		dirs = append(dirs, findCandidates(subdir, minSize)...)
	}
	return dirs
}
