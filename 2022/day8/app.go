package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var input []string
	for fileScanner.Scan() {
		input = append(input, fileScanner.Text())
	}
	readFile.Close()

	forest := make([][]int, len(input))
	for i, v := range input {
		t := strings.Split(v, "")
		forest[i] = make([]int, 0)
		for _, s := range t {
			d, _ := strconv.Atoi(s)
			forest[i] = append(forest[i], d)
		}
	}

	cols := make([][]int, len(forest[0]))
	for i := 0; i < len(forest[0]); i++ {
		cols[i] = make([]int, 0)
		for j := 0; j < len(forest); j++ {
			cols[i] = append(cols[i], forest[j][i])
		}
	}
	visible := 0

	for ri, row := range forest {
		for ti, tree := range row {
			if ri == 0 || ti == 0 || ri == len(forest)-1 || ti == len(row)-1 {
				visible++
			} else if isVisible(row[ti+1:], tree) {
				// r
				visible++
			} else if isVisible(row[:ti], tree) {
				// l
				visible++
			} else if isVisible(cols[ti][ri+1:], tree) {
				// d
				visible++
			} else if isVisible(cols[ti][:ri], tree) {
				// u
				visible++
			}
		}
	}
	fmt.Println("part1", visible)

}

func isVisible(row []int, n int) bool {
	for _, v := range row {
		if v >= n {
			return false
		}
	}
	return true
}
