package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/**
Monkey 0:
  Starting items: 63, 57
  Operation: new = old * 11
  Test: divisible by 7
    If true: throw to monkey 6
    If false: throw to monkey 2
*/

type Monkey struct {
	id         int
	Task       Task
	Historical []Task
	Processed  int
	Items      []Item
}

type Item struct {
	id, worry int
}

type Task struct {
	Monkey, DivisibleBy, IfTrue, IfFalse, Old, Worry int
	Operation                                        Operation
	Items                                            []int
}

type Operation struct {
	Left, Right, Operand string
}

const (
	P = "*"
	A = "+"
)

var round int

func main() {
	readFile, err := os.Open("test1.txt")
	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var lineCounter int
	var monkey, item, divisibleBy, iftrue, iffalse int
	var itemz []int
	var task Task
	var worker Monkey
	var monkeys []Monkey
	for fileScanner.Scan() {
		value := fileScanner.Text()
		if lineCounter <= 5 {
			//Monkey 0:  Starting items: 63, 57  Operation: new = old * 11  Test: divisible by 7    If true: throw to monkey 6    If false: throw to monkey 2
			if strings.Contains(value, "Monkey") {
				_, err := fmt.Sscanf(value, "Monkey %d:", &monkey)
				if err != nil {
					fmt.Println(err)
				}
				task.Monkey = monkey
				worker = Monkey{id: monkey}
			}

			if strings.Contains(value, "Starting") {
				lados := strings.Split(value, ":")
				if strings.Contains(lados[1], ",") {
					str := strings.Replace(lados[1], " ", "", -1)
					strs := strings.Split(str, ",")
					for i := range strs {
						item, _ = strconv.Atoi(strs[i])
						itemz = append(itemz, item)
					}
				} else {
					str := strings.Replace(lados[1], " ", "", -1)
					item, _ = strconv.Atoi(str)
					itemz = append(itemz, item)
				}
				task.Items = itemz
			}

			if strings.Contains(value, "Operation") {
				var operation Operation
				lados1 := strings.Split(value, ":")
				_, err := fmt.Sscanf(lados1[1], " new = %s %s %s", &operation.Left, &operation.Operand, &operation.Right)
				if err != nil {
					fmt.Println(err)
				}
				task.Operation = operation
			}
			if strings.Contains(value, "Test") {
				_, err := fmt.Sscanf(value, "  Test: divisible by %d", &divisibleBy)
				if err != nil {
					fmt.Println(err)
				}
				task.DivisibleBy = divisibleBy
			}
			if strings.Contains(value, "If true:") {
				_, err := fmt.Sscanf(value, "    If true: throw to monkey %d", &iftrue)
				if err != nil {
					fmt.Println(err)
				}
				task.IfTrue = iftrue
			}

			if strings.Contains(value, "If false:") {
				_, err := fmt.Sscanf(value, "    If false: throw to monkey %d", &iffalse)
				if err != nil {
					fmt.Println(err)
				}
				task.IfFalse = iffalse
			}

			lineCounter++
		} else {
			// process(task)
			// tasks = append(tasks, task)
			worker.Task = task
			monkeys = append(monkeys, worker)
			lineCounter = 0
			task = Task{}
			itemz = []int{}
			worker = Monkey{}
		}
	}
	readFile.Close()
	work(monkeys)
}

func work(monks []Monkey) {
	for i, monk := range monks {
		for _, item := range monk.Task.Items {
			var oldP, newP int
			if monks[i].Task.Operation.Left == "old" {
				oldP = item
			} else {
				newP, _ = strconv.Atoi(monks[i].Task.Operation.Left)
			}
			if monks[i].Task.Operation.Right == "old" {
				newP = item
			} else {
				newP, _ = strconv.Atoi(monks[i].Task.Operation.Right)
			}
			worryLevel := operate(oldP, newP, monks[i].Task.Operation.Operand)
			var relief int
			relief = worryLevel / 3
			var it Item
			it.id = item
			it.worry = relief
			monks[i].Items = append(monks[i].Items, it)
			monks[i].Task.Worry = relief
			if relief%monks[i].Task.DivisibleBy == 0 {
				monks = addToMonkeyThisTask(monks[i].Task.IfTrue, relief, monks, it)
				// fmt.Printf("Job sent to monkey, %d \n", task.IfTrue)
			} else {
				monks = addToMonkeyThisTask(monks[i].Task.IfFalse, relief, monks, it)
				// fmt.Printf("Job sent to monkey, %d \n", task.IfFalse)
			}
			monks[i].Historical = append(monks[i].Historical, monks[i].Task)
			monks[i].Task.Items = monks[i].Task.Items[1:]
			monks[i].Items = monks[i].Items[1:]
			monks[i].Processed++
			// monks[i] = monk
		}
	}
	if shouldWeContinue(monks, round) {
		round++
		// printRound(monks)
		fmt.Printf("Round %d \n", round)
		work(monks)
	} else {
		for _, mon := range monks {
			fmt.Printf("Monkey %d inspected items %d times. \n", mon.id, mon.Processed)
		}
	}
}

func printRound(monks []Monkey) {
	for _, monk := range monks {
		fmt.Printf("Monkey %d  Items: %v  \n", monk.id, monk.Items)
	}
}

func addToMonkeyThisTask(id int, item int, monks []Monkey, it Item) []Monkey {

	monks[id].Task.Items = append(monks[id].Task.Items, item)
	monks[id].Items = append(monks[id].Items, it)
	return monks
}
func operate(old, new int, operand string) int {
	switch operand {
	case P:
		return old * new
	case A:
		return old + new
	}
	return 0
}

func shouldWeContinue(worker []Monkey, round int) bool {
	var option bool
	if round == 19 {
		return false
	}
	for _, monk := range worker {
		if len(monk.Task.Items) > 0 {
			return true
		} else {
			option = false
		}
	}
	return option
}
