package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l.

	A present with 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
	A present with 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.

	3x11x24
	13x5x19
	1x9x27
	24x8x21
	6x8x17
	*/
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var paper int = 0
	for fileScanner.Scan() {
		//Split by x
		values := fileScanner.Text()
		sides := strings.Split(values, "x")
		l, err := strconv.Atoi(sides[0])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, sides[0])
		}
		w, err := strconv.Atoi(sides[1])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, sides[1])
		}
		h, err := strconv.Atoi(sides[2])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, sides[2])
		}
		side1 := l * w
		side2 := w * h
		side3 := h * l
		var min int = 0
		for side1 > 0 && side2 > 0 && side3 > 0 {
			side1--
			side2--
			side3--
			min++
		}
		fmt.Printf("We have %s then we have a h: %d, w: %d , l: %d with a surface of %d a side1: %d a side2: %d and a side3: %d and a min of %d \n", values, l, w, h, 2*l*w+2*w*h+2*h*l, l*w, w*h, h*l, min)
		paper += (2*l*w + 2*w*h + 2*h*l + min)
	}
	readFile.Close()
	fmt.Printf("Looks like you have to order %d", paper)
}
