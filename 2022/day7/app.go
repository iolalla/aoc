package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

type file struct {
	name   string
	size   int
	isDir  bool
	parent *file
	files  []*file
}

func (f *file) addFile(name string, size int) {
	f.files = append(f.files, &file{name: name, parent: f, size: size})
	f.increaseSize(size)
}

func (f *file) addDir(name string) {
	f.files = append(f.files, &file{name: name, parent: f, files: []*file{}, isDir: true})
}

func (f *file) increaseSize(size int) {
	f.size += size
	if f.parent != nil {
		f.parent.increaseSize(size)
	}
}

func (f *file) findDir(name string) *file {
	for _, d := range f.files {
		if d.name == name && d.isDir {
			return d
		}
	}
	return nil
}

var paths = make(map[string]int)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 *
	 */
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var lastPath string
	var lastDir string
	root := &file{name: "/", files: []*file{}}
	current := root
	for fileScanner.Scan() {
		//
		var size int
		var name string
		var dir string
		value := fileScanner.Text()
		if strings.HasPrefix(value, "$ cd") {
			fmt.Sscanf(value, "$ cd  %s", &dir)
			if dir == "/" {
				current = root
				// continue
			} else if dir == ".." {
				lastPath = lastPath + "/" + dir
				lastDir = dir
				current = current.parent
			} else {
				current = current.findDir(dir)
				lastPath = strings.Replace(lastPath, "/"+lastDir, "", -1)
			}
		}
		if strings.HasPrefix(value, "dir ") {
			fmt.Sscanf(value, "dir %s", &dir)
			current.addDir(dir)
		}
		if unicode.IsDigit(rune(value[0])) {
			fmt.Sscanf(value, "%d %s", &size, &name)
			//fmt.Printf("%s , %d \n", lastPath, size)
			add(lastPath, size)
			current.addFile(name, size)
		}
	}
	readFile.Close()
	var total int
	for key, sicito := range paths {
		if sicito <= 100000 {
			fmt.Printf("%s , %d \n", key, sicito)
			total += sicito
		}
	}
	fmt.Printf("Answer: %d", findCandidatesFileSize(root))
}

func add(lastPath string, size int) {
	patitos := strings.Split(lastPath, "/")
	for _, pazito := range patitos[:len(patitos)-1] {
		if pazito != "" {
			oldsize := paths[pazito]
			paths[pazito] = oldsize + size
		}
	}
}

func findCandidatesFileSize(dir *file) int {
	total := 0

	if dir.size <= 100000 {
		total += dir.size
	}
	for _, file := range dir.files {
		if file.isDir {
			total += findCandidatesFileSize(file)
		}
	}
	return total
}
