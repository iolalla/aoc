package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	The ribbon required to wrap a present is the shortest distance around its sides, or the smallest perimeter of any one face. Each present also requires a bow made out of ribbon as well; the feet of ribbon required for the perfect bow is equal to the cubic feet of volume of the present. Don't ask how they tie the bow, though; they'll never tell.

	For example:

	A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet.
	A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet.

	3x11x24
	13x5x19
	1x9x27
	24x8x21
	6x8x17
	*/
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var ribbon int = 0
	for fileScanner.Scan() {
		//Split by x
		values := fileScanner.Text()
		var w, h, l int
		fmt.Sscanf(values, "%dx%dx%d", &l, &w, &h)

		side1 := l * w
		side2 := w * h
		side3 := h * l
		var min int = 0
		for side1 > 0 && side2 > 0 && side3 > 0 {
			side1--
			side2--
			side3--
			min++
		}
		// fmt.Printf("We have %s then we have a h: %d, w: %d , l: %d with a surface of %d a side1: %d a side2: %d and a side3: %d and a min of %d \n", values, l, w, h, 2*l*w+2*w*h+2*h*l, l*w, w*h, h*l, min)
		i, j := getSmaller(w, h, l)
		ribbon += ((w * h * l) + (i + i + j + j))
	}
	readFile.Close()
	fmt.Printf("Looks like you have to order %d", ribbon)
}

// 1588178 wrong

func getSmaller(w int, h int, l int) (int, int) {
	list := []int{w, h, l}
	sort.Ints(list)
	return list[0], list[1]

}
