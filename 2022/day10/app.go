package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
noop
addx 3
addx -5
*/

const (
	noop = "noop"
	addx = "addx"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var cycles []int
	var instruction string
	var totalSignal, cycle, signal, multiplier, twenties, first int
	first = 0
	multiplier = 20
	for fileScanner.Scan() {
		value := fileScanner.Text()
		if strings.Contains(value, noop) {
			instruction = noop
			if first == 0 {
				signal = 1
				first = 1
			} else {
				signal = 0
			}
		} else if strings.Contains(value, addx) {
			instruction = addx
			fmt.Sscanf(value, "addx %d", &signal)
		} else {
			fmt.Println("Wrong instruction ", value)
			os.Exit(0)
		}

		switch instruction {
		case noop:
			cycle++
			if cycle == multiplier {
				twenties += totalSignal * multiplier
				//twenties += signal
				multiplier += 40
			}
			totalSignal = totalSignal + signal
			cycles = append(cycles, totalSignal)
		case addx:
			cycle++
			if cycle == multiplier {
				twenties += totalSignal * multiplier
				//twenties += signal
				multiplier += 40
			}
			cycles = append(cycles, totalSignal)
			cycle++
			if cycle == multiplier {
				twenties += totalSignal * multiplier
				//twenties += signal
				multiplier += 40
			}
			totalSignal = totalSignal + signal
			cycles = append(cycles, totalSignal)
		}
	}
	fmt.Printf("Response: %d , %d , %d, %d \n", len(cycles), totalSignal, cycles, twenties)
	readFile.Close()
}
