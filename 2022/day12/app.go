package main

import (
	"fmt"
	"os"
	"strings"
)

/*
Solution Source: https://github.com/mnml/aoc/blob/main/2022/12/1.go
*

Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
*/
type Point struct {
	X, Y int
}

func (p Point) Add(q Point) Point {
	return Point{p.X + q.X, p.Y + q.Y}
}

func main() {
	input, err := os.ReadFile("input.txt")
	if err != nil {
		fmt.Println(err)
	}
	var start, end Point
	height := map[Point]rune{}
	for x, s := range strings.Fields(string(input)) {
		for y, r := range s {
			height[Point{x, y}] = r
			if r == 'S' {
				start = Point{x, y}
			} else if r == 'E' {
				end = Point{x, y}
			}
		}
	}
	height[start], height[end] = 'a', 'z'

	dist := map[Point]int{end: 0}
	queue := []Point{end}
	var shortest *Point
	// Let's do this backwards from the Highest point to the lowest point
	for len(queue) > 0 {
		cur := queue[0]
		queue = queue[1:]
		// Init of the inital valuwe
		if height[cur] == 'a' && shortest == nil {
			shortest = &cur
		}
		//Pretty simple we look to the four points around each point, Down, Up, Right and Left
		for _, d := range []Point{{0, -1}, {1, 0}, {0, 1}, {-1, 0}} {
			// For each of those we add the point and compare the height, looking for one lower or equal
			next := cur.Add(d)
			_, seen := dist[next]
			_, valid := height[next]

			if !seen && valid && height[cur] <= height[next]+1 {
				dist[next] = dist[cur] + 1
				queue = append(queue, next)
			}
		}
	}

	fmt.Printf("Part1: %d \n", dist[start])     // test: 31 input: 383
	fmt.Printf("Part2: %d \n", dist[*shortest]) // test: 29 input: 377
}
