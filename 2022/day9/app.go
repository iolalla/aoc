package main

import (
	"bufio"
	"fmt"
	"os"
)

type Field struct {
	Length, Wide         int
	Head, Tail, Start    position
	HistoryTailPositions []position
}

type position struct {
	x, y  int
	value string
}

func (f *Field) addPosition(pos position) {
	f.HistoryTailPositions = append(f.HistoryTailPositions, pos)
}

func (f *Field) paint() {
	for j := 0; j < f.Length; j++ {
		for i := 0; i < f.Wide; i++ {
			fmt.Print(".")
		}
		fmt.Print("\n")
	}
}

type move struct {
	order     int
	direction string
	steps     int
}

/*
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
*/

func main() {
	readFile, err := os.Open("test2.txt")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var moves []move
	var counter int
	var zteps int
	head := position{x: 0, y: 0, value: "H"}
	tail := position{x: 0, y: 0, value: "T"}
	start := position{x: 0, y: 0, value: "s"}
	var campo Field
	campo.Head = head
	campo.Start = start
	campo.Tail = tail
	campo.Wide = 6
	campo.Length = 5
	for fileScanner.Scan() {
		value := fileScanner.Text()
		var direction string
		var steps int
		fmt.Sscanf(value, "%s %d", &direction, &steps)
		moves = append(moves, move{direction: direction, steps: steps, order: counter})
		campo = moveHead(move{direction: direction, steps: steps, order: counter}, campo)
		counter++
		zteps = zteps + steps
	}
	/**
	How many positions does the tail of the rope visit at least once?
	*/
	// func to create the field
	// Define field size 50x50
	// Put 50 points x 50 points
	// Put Head in a position : 1x1
	// Add method PaintState
	// Add method move
	// func able to move the head step by step
	// func able to move the tail
	// func able to represent the head
	// add tail to representation
	fmt.Printf("Response: %d \n", len(removeDuplicateValues(campo.HistoryTailPositions)))
	fmt.Printf("Steps: %d \n", zteps)
	campo.paint()
	readFile.Close()
}

func moveHead(move move, campo Field) Field {
	for i := 0; i < move.steps; i++ {
		if move.direction == "R" {
			campo.Head.x = campo.Head.x + 1
			if tailMoves(campo) {
				campo = moveTail(move, campo)
			}
		} else if move.direction == "L" {
			campo.Head.x = campo.Head.x - 1
			if tailMoves(campo) {
				campo = moveTail(move, campo)
			}
		} else if move.direction == "U" {
			campo.Head.y = campo.Head.y + 1
			if tailMoves(campo) {
				campo = moveTail(move, campo)
			}
		} else if move.direction == "D" {
			campo.Head.y = campo.Head.y - 1
			if tailMoves(campo) {
				campo = moveTail(move, campo)
			}
		}
	}
	return campo
}

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func moveTail(move move, campo Field) Field {
	xdistance := Abs(campo.Head.x - campo.Tail.x)
	ydistance := Abs(campo.Head.y - campo.Tail.y)
	if (xdistance >= 1 || ydistance >= 1) && (xdistance != 0 && ydistance != 0) {
		if campo.Head.x > campo.Tail.x && campo.Head.y > campo.Tail.y {
			campo.Tail.x = campo.Tail.x + 1
			campo.Tail.y = campo.Tail.y + 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		} else if campo.Head.x < campo.Tail.x && campo.Head.y > campo.Tail.y {
			campo.Tail.x = campo.Tail.x - 1
			campo.Tail.y = campo.Tail.y + 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		} else if campo.Head.x < campo.Tail.x && campo.Head.y < campo.Tail.y {
			campo.Tail.x = campo.Tail.x - 1
			campo.Tail.y = campo.Tail.y - 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		} else if campo.Head.x > campo.Tail.x && campo.Head.y < campo.Tail.y {
			campo.Tail.x = campo.Tail.x + 1
			campo.Tail.y = campo.Tail.y - 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		}
	} else if xdistance == 0 {
		if campo.Head.y < campo.Tail.y {
			campo.Tail.y = campo.Tail.y - 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		} else if campo.Head.y > campo.Tail.y {
			campo.Tail.y = campo.Tail.y + 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		}
	} else if ydistance == 0 {
		if campo.Head.x < campo.Tail.x {
			campo.Tail.x = campo.Tail.x - 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		} else if campo.Head.x > campo.Tail.x {
			campo.Tail.x = campo.Tail.x + 1
			campo.addPosition(position{x: campo.Tail.x, y: campo.Tail.y, value: "T"})
		}
	} else {
		fmt.Printf("How is that possible?, %v , %v \n", move, campo)
	}
	return campo
}

func removeDuplicateValues(intSlice []position) []position {
	keys := make(map[position]bool)
	list := []position{}

	// If the key(values of the slice) is not equal
	// to the already present value in new slice (list)
	// then we append it. else we jump on another element.
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func tailMoves(campo Field) bool {
	xdistance := campo.Head.x - campo.Tail.x
	ydistance := campo.Head.y - campo.Tail.y
	if Abs(xdistance) > 1 || Abs(ydistance) > 1 {
		return true
	}
	return false
}
