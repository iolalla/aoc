package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)
	//Let's define the array of elfs to keep the information
	var elfs []int
	//This is the elf that will be reused in every group of calory data
	var elf int = 0
	for fileScanner.Scan() {
		value := fileScanner.Text()
		if value != "" {
			intVar, err := strconv.Atoi(value)
			if err != nil {
				fmt.Println(err)
			}
			elf = elf + intVar
		} else {
			elfs = append(elfs, elf)
			elf = 0
		}
	}
	readFile.Close()
	// Let's sort the elfs based in the calories they have
	sort.Ints(elfs[:])
	// Let's see the elf with more calories
	fmt.Printf("FirstPart: %d \n", elfs[len(elfs)-1])
	// Let's calculate the last three elfs calories
	var secondPart int
	for _, elf := range elfs[len(elfs)-3:] {
		secondPart = secondPart + elf
	}
	fmt.Printf("SecondPart: %d \n", secondPart)
}
