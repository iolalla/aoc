package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 *Some of the pairs have noticed that one of their assignments fully contains the other.
	For example, 2-8 fully contains 3-7, and 6-6 is fully contained by 4-6.
	5-7,7-9
	....567..  5-7
	......789  7-9
	2-8,3-7
	.2345678.  2-8
	..34567..  3-7
	21-52,36-51
	36-38,37-96
	*/
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var repeated int = 0
	for fileScanner.Scan() {
		//Split by ,
		//Split each part by -
		values := fileScanner.Text()
		halfs := strings.Split(values, ",")
		lettersA := strings.Split(halfs[0], "-")
		lettersB := strings.Split(halfs[1], "-")
		A, err := strconv.Atoi(lettersA[0])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, lettersA[0])
		}
		B, err := strconv.Atoi(lettersA[1])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, lettersA[1])
		}
		C, err := strconv.Atoi(lettersB[0])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, lettersB[0])
		}
		D, err := strconv.Atoi(lettersB[1])
		if err != nil {
			fmt.Printf("Casting Error, %v, with integer %s ", err, lettersB[1])
		}
		if A <= C && B >= C {
			repeated++
		} else if A <= D && B >= D {
			repeated++
		} else if C <= A && D >= B {
			repeated++
		} else if C <= B && D >= A {
			repeated++
		}
	}
	readFile.Close()
	fmt.Printf("Looks like we have finally those sections repeated %d", repeated)
}
