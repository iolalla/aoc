package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 * Three lines 1 rucksack
	 * The char in the three lines is the badge
	 *
	 */
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var priority int = 0
	var lines []string
	for fileScanner.Scan() {
		line := fileScanner.Text()
		lines = append(lines, line)
	}
	readFile.Close()
	size := len(lines)
	var line1 []string
	for i := 1; i < size; i = i + 3 {
		if i-1 > 0 {
			line1 = lines[i-1 : i]
		} else {
			line1 = lines[0:i]
		}
		line2 := lines[i : i+1]
		line3 := lines[i+1 : i+2]
		for _, zar := range line1[0] {
			//Find repeated char
			if strings.Contains(line2[0], string(zar)) && strings.Contains(line3[0], string(zar)) {
				//Calculate pripority
				if unicode.IsUpper(zar) {
					priority = priority + int(zar) - int('A') + 27
					fmt.Printf("We re adding a priority for char: %s  of %d minus %d for int(A) and final priority of %d \n", string(zar), int(zar), int('A'), priority)
				} else if unicode.IsLower(zar) {
					priority = priority + int(zar) - int('a') + 1
					fmt.Printf("We re adding a priority for char: %s of %d minus %d for int(a) and final priority of %d \n", string(zar), int(zar), int('a'), priority)
				}
				fmt.Printf("Looks we have char %s in the string %s with number %d \n", string(zar), line2, zar)
				break
			}
		}
	}
	//Lowercase item types a through z have priorities 1 through 26.
	//Uppercase item types A through Z have priorities 27 through 52.
	fmt.Printf("Looks like we have a final priority of %d", priority)
}
