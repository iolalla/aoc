package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
	"strings"
	"unicode"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 * Lowercase item types a through z have priorities 1 through 26.
	 * Uppercase item types A through Z have priorities 27 through 52.
	 * vJrwpWtwJgWrhcsFMMfFFhFp --> vJrwpWtwJgWr , hcsFMMfFFhFp
	 * hcsFMMfFFhFp
	 * vJrwpWtwJgWr
	 * jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL --> jqHRNqRjqzjGDLGL , rsFMfFZSrLrFZsSL
	 * rsFMfFZSrLrFZsSL
	 * jqHRNqRjqzjGDLGL
	 * PmmdzqPrVvPwwTWBwg --> PmmdzqPrV , vPwwTWBwg
	 * vPwwTWBwg
	 * PmmdzqPrV
	 */
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var priority int = 0
	for fileScanner.Scan() {
		//Split by half
		values := fileScanner.Text()
		size := len(values)
		half := size / 2
		if reflect.TypeOf(half).Name() != "int" {
			fmt.Printf("%q looks like it's not a int .\n", half)
		}
		half1 := values[:half]
		half2 := values[half:]
		fmt.Printf("%s has two parts %s %s both of size %d \n", values, half1, half2, half)
		for _, zar := range half1 {
			//Find repeated char
			if strings.Contains(half2, string(zar)) {
				//Calculate pripority
				if unicode.IsUpper(zar) {
					priority = priority + int(zar) - int('A') + 27
					fmt.Printf("We re adding a priority for char: %s  of %d minus %d for int(A) and final priority of %d \n", string(zar), int(zar), int('A'), priority)
				} else if unicode.IsLower(zar) {
					priority = priority + int(zar) - int('a') + 1
					fmt.Printf("We re adding a priority for char: %s of %d minus %d for int(a) and final priority of %d \n", string(zar), int(zar), int('a'), priority)
				}
				fmt.Printf("Looks we have char %s in the string %s with number %d \n", string(zar), half2, zar)
				break
			}
		}
		//Lowercase item types a through z have priorities 1 through 26.
		//Uppercase item types A through Z have priorities 27 through 52.
	}
	readFile.Close()
	fmt.Printf("Looks like we have a final priority of %d", priority)
}
