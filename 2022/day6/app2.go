package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 *
	 */
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanRunes)
	var counter int
	var zars []int32
	for fileScanner.Scan() {
		//
		values := fileScanner.Text()
		for _, zar := range values {
			if len(zars) == 14 {
				fmt.Printf(" This is the counter %d \n ", counter)
				os.Exit(0)
			} else if len(zars) == 0 {
				zars = append(zars, zar)
				break
			}
			for _, zio := range zars {
				if zar == zio {
					zars = []int32{}
					fmt.Printf("Found reset letter: %d from %s \n", zar, string(zar))
					break
				}
			}
			zars = append(zars, zar)
			fmt.Printf("Added another letter: %d from %s \n", zar, string(zar))
			counter++
		}
	}
	readFile.Close()
}
