package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	// Read File crates
	readFile, err := os.Open("crates_ordered.txt")

	if err != nil {
		fmt.Println(err)
	}
	/**
	 *They do, however, have a drawing of the starting stacks of crates and the rearrangement procedure
	(your puzzle input). For example:

	    [D]
	[N] [C]
	[Z] [M] [P]
	 1   2   3

	move 1 from 2 to 1
	move 3 from 1 to 3
	move 2 from 2 to 1
	move 1 from 1 to 2
	In this example, there are three stacks of crates. Stack 1 contains two crates: crate Z is on the bottom, and crate
	N is on top. Stack 2 contains three crates; from bottom to top, they are crates M, C, and D. Finally, stack 3
	contains a single crate, P.

	Then, the rearrangement procedure is given. In each step of the procedure, a quantity of crates is moved from one
	stack to a different stack. In the first step of the above rearrangement procedure, one crate is moved from stack 2 to stack 1, resulting in this configuration:

	[D]
	[N] [C]
	[Z] [M] [P]
	 1   2   3
	In the second step, three crates are moved from stack 1 to stack 3. Crates are moved one at a time, so the first
	crate to be moved (D) ends up below the second and third crates:

	        [Z]
	        [N]
	    [C] [D]
	    [M] [P]
	 1   2   3
	Then, both crates are moved from stack 2 to stack 1. Again, because crates are moved one at a time, crate C ends up
	below crate M:

	        [Z]
	        [N]
	[M]     [D]
	[C]     [P]
	 1   2   3
	Finally, one crate is moved from stack 1 to stack 2:

	        [Z]
	        [N]
	        [D]
	[C] [M] [P]
	 1   2   3
	The Elves just need to know which crate will end up on top of each stack; in this example, the top crates are C in
	stack 1, M in stack 2, and Z in stack 3, so you should combine these together and give the Elves the message CMZ.

	*/
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var crates [][]string
	for fileScanner.Scan() {
		// Split by " "
		// Remove [ and ]
		// Build 9 arrays
		// Load the 9 arrays with the letters
		values := fileScanner.Text()
		values = strings.ReplaceAll(values, "[", "")
		values = strings.ReplaceAll(values, "]", "")
		halves := strings.Split(values, ",")
		var crate []string
		for _, val := range halves {
			crate = append(crate, val)
		}
		crates = append(crates, crate)
		fmt.Printf("Tenemos una linea de %d caracteres %v \n", len(halves), halves)
	}
	readFile.Close()

	// Load the data from moves.txt
	fmt.Printf(" This is the array of crates %v \n ", crates)

	readFile2, err2 := os.Open("moves.txt")
	if err2 != nil {
		fmt.Println(err2)
	}
	fileScanner2 := bufio.NewScanner(readFile2)
	fileScanner2.Split(bufio.ScanLines)
	/**
	move 1 from 7 to 6
	move 1 from 9 to 4
	move 4 from 9 to 6
	*/
	for fileScanner2.Scan() {
		move := fileScanner2.Text()
		var amount int
		var from int
		var to int
		fmt.Sscanf(move, "move %d from %d to %d", &amount, &from, &to)
		to = to - 1
		from = from - 1
		source := crates[from]
		destination := crates[to]
		n := len(source) - amount
		for _, r := range source[n:] {
			destination = append(destination, r)
		}
		source = source[:n]
		crates[from] = source
		crates[to] = destination
	}
	readFile2.Close()

	response := ""
	stackKeys := [9]int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	for _, k := range stackKeys {
		response += string(crates[k][len(crates[k])-1])
	}
	fmt.Println(response)
	fmt.Printf(" This is the array of crates %v \n ", crates)

}
