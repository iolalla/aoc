package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func numVisible(row []int, n int, reverse bool) int {
	count := 0
	if reverse {
		for i := len(row) - 1; i >= 0; i-- {
			count++
			if row[i] >= n {
				return count
			}
		}
	} else {
		for _, v := range row {
			count++
			if v >= n {
				return count
			}
		}
	}

	return count
}

func main() {
	f, _ := os.Open("../input.txt")
	scanner := bufio.NewScanner(f)
	var input []string
	for scanner.Scan() {
		input = append(input, scanner.Text())
	}

	forest := make([][]int, len(input))
	for i, v := range input {
		t := strings.Split(v, "")
		forest[i] = make([]int, 0)
		for _, s := range t {
			d, _ := strconv.Atoi(s)
			forest[i] = append(forest[i], d)
		}
	}

	cols := make([][]int, len(forest[0]))
	for i := 0; i < len(forest[0]); i++ {
		cols[i] = make([]int, 0)
		for j := 0; j < len(forest); j++ {
			cols[i] = append(cols[i], forest[j][i])
		}
	}

	max := 0
	for ri, row := range forest {
		for ti, tree := range row {

			right := numVisible(row[ti+1:], tree, false)      // r
			left := numVisible(row[:ti], tree, true)          // l
			below := numVisible(cols[ti][ri+1:], tree, false) // d
			above := numVisible(cols[ti][:ri], tree, true)    // u
			score := right * left * below * above

			if score > max {
				max = score
			}
		}
	}
	fmt.Println("Solution: ", max)
}
