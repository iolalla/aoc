package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	readFile, err := os.Open("input.txt")

	if err != nil {
		fmt.Println(err)
	}

	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var input []string
	for fileScanner.Scan() {
		input = append(input, fileScanner.Text())
	}
	readFile.Close()

	commands := strings.Split(input[0], "")
	houses := map[string]bool{"0-0": true}
	coords := [][2]int{{0, 0}, {0, 0}}

	for i, c := range commands {
		sr := 0
		if i%2 == 0 {
			sr = 1
		}
		y, x := coords[sr][0], coords[sr][1]

		switch c {
		case "^":
			y--
		case ">":
			x++
		case "v":
			y++
		case "<":
			x--
		}

		coords[sr] = [2]int{y, x}
		houses[fmt.Sprintf("%d-%d", y, x)] = true
	}

	fmt.Printf("Solution : %v\n", len(houses))

}
